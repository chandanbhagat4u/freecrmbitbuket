package com.signup;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.python.icu.impl.duration.TimeUnit;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class MethodRepositry {
	static WebDriver driver;

	//Code is added to Bitbucket
	public static void browserLaunch() {

		// // Normal way to launch the website.
		// System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		// WebDriver driver = new ChromeDriver();
		// driver.get("https://www.freecrm.com/register/");

		// Easy way to launch the website.
		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.freecrm.com/register/");
		driver.manage().window().maximize();
	}

	public static void varifyValidSignup() throws InterruptedException, AWTException, FindFailed {
		// Select Class for select the value from the dropdown
		WebElement Edition = driver.findElement(By.xpath("//select[@id='payment_plan_id']"));
		Select dropdown = new Select(Edition);
		dropdown.selectByVisibleText("Free Edition");
		Thread.sleep(3000);
		dropdown.selectByIndex(2);
		Thread.sleep(3000);
		dropdown.selectByVisibleText("Free Edition");
		Thread.sleep(1000);

		// Using Robot class for move to next fields
		Robot tabObj = new Robot();
		tabObj.keyPress(KeyEvent.VK_TAB);
		tabObj.keyRelease(KeyEvent.VK_TAB);

		// Using Action Class to enter the First name last name
		WebElement FirstName = driver.findElement(By.xpath("//input[@placeholder='First Name']"));
		Actions builder = new Actions(driver);
		builder.moveToElement(FirstName).click().sendKeys("CRM").build().perform();

		WebElement LastName = driver.findElement(By.xpath("//input[@placeholder='Last Name']"));
		Actions builder1 = new Actions(driver);
		builder1.moveToElement(LastName).click().sendKeys("Four").build().perform();

		// Using Robot class for move to next fields
		Robot tabObj1 = new Robot();
		tabObj1.keyPress(KeyEvent.VK_TAB);
		tabObj1.keyRelease(KeyEvent.VK_TAB);

		// Using Sikuli for email filed
		Screen SikuliScreen = new Screen();
		Thread.sleep(3000);
		Pattern email = new Pattern("./Sikuli/UpdateEmail.png");
		SikuliScreen.wait(email, 10);
		SikuliScreen.type("freecrm10test@mailinator.com");

		// Using Java script executor for confirm email field
		WebElement ConfirmEmail = driver.findElement(By.name("email_confirm"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].value='freecrm10test@mailinator.com';", ConfirmEmail);
		Thread.sleep(3000);

		WebElement Username = driver.findElement(By.name("username"));
		JavascriptExecutor un = (JavascriptExecutor) driver;
		un.executeScript("arguments[0].value='freecrm10test'", Username);

		WebElement Password = driver.findElement(By.name("password"));
		JavascriptExecutor Pwd = (JavascriptExecutor) driver;
		Pwd.executeScript("arguments[0].value='123456'", Password);

		WebElement ConfirmPassword = driver.findElement(By.name("passwordconfirm"));
		JavascriptExecutor ConfPwd = (JavascriptExecutor) driver;
		ConfPwd.executeScript("arguments[0].value='123456'", ConfirmPassword);

		// Using Sikuli for checkbox filed
		Screen screen = new Screen();
		Thread.sleep(3000);
		Pattern checkbox = new Pattern("./Sikuli/Condition.png");
		screen.wait(checkbox, 10);
		screen.click();

		// Using robot class for scrolling
		Robot scroll = new Robot();
		scroll.mouseWheel(3);

		// Click on Submit button using Sikuli
		Pattern submit = new Pattern("./Sikuli/SubmitButton.png");
		screen.wait(submit, 10);
		screen.click();

		// Wait For Page To Load
		Thread.sleep(3000);
		WebElement Checktext = driver.findElement(By.xpath("//div[@class='text_orange']"));
		String Actualtext = Checktext.getText();
		System.out.println("Heading text is - " + Actualtext);
		String ExpectedText = "Your company information and profile detail";

		// Checking conditions
		if (ExpectedText.equals(Actualtext)) {
			System.out.println("Sign up of first step is PASSED");
		} else {
			System.out.println("Sign up of first step is FAILED");
		}

	}

	public static void signUpStep2() throws AWTException, InterruptedException, FindFailed {
		// Java Script Executor
		WebElement Company = driver.findElement(By.id("company_name"));
		JavascriptExecutor CompanyName = (JavascriptExecutor) driver;
		CompanyName.executeScript("arguments[0].value='Test Company 4'", Company);

		// Action Class
		WebElement PhoneNumber = driver.findElement(By.id("phone"));
		Actions PN = new Actions(driver);
		PN.moveToElement(PhoneNumber).click().sendKeys("9087654321").build().perform();

		// Robot Class
		Robot scrolldown = new Robot();
		scrolldown.mouseWheel(3);

		// Select Class
		WebElement CountrySelect = driver.findElement(By.name("country"));
		Select CountryDropdown = new Select(CountrySelect);
		CountryDropdown.selectByVisibleText("India");

		// Sikuli
		Screen Screen = new Screen();
		Pattern checkbox = new Pattern("./Sikuli/BillingAddress.png");
		Screen.wait(checkbox, 10);
		Screen.click();

		// Javascript executor
		WebElement SubmitButton = driver.findElement(By.xpath("//button[@name='btnSubmit']"));
		JavascriptExecutor SB = (JavascriptExecutor) driver;
		SB.executeScript("arguments[0].click();", SubmitButton);
		Thread.sleep(6000);		
	}
	
	public static void signUpStep3() throws AWTException, InterruptedException {
		Robot Scrolling = new Robot();
		//Scrolling.mouseWheel(2);
		Scrolling.keyPress(KeyEvent.VK_PAGE_DOWN);
		Scrolling.keyRelease(KeyEvent.VK_PAGE_DOWN);
		
		WebElement CompleteRegistration = driver.findElement(By.name("finish"));
		JavascriptExecutor CR = (JavascriptExecutor) driver;
		CR.executeScript("arguments[0].click();", CompleteRegistration);
		
		WebElement Registered = driver.findElement(By.className("text_orange"));
		String ActualText = Registered.getText();
		System.out.println("Actual text is showing - "+ActualText);
		String ExpectedText = "Your account is now registered.";
		
		// Check the page title
		if (ExpectedText.equals(ActualText)) {
			System.out.println("Sucessfull login- PASSED");
		} else {
			System.out.println("Unsucessfull login - FAILED");
		}
	}

		
}
