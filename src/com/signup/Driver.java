package com.signup;

import java.awt.AWTException;

import org.sikuli.script.FindFailed;

public class Driver {

	public static void main(String[] args) throws InterruptedException, AWTException, FindFailed {
		// TODO Auto-generated method stub
		MethodRepositry MR = new MethodRepositry();
		MR.browserLaunch();
		MR.varifyValidSignup();
		MR.signUpStep2();
		MR.signUpStep3();

	}

}
